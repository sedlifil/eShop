package cz.cvut.eshop.archive;

import cz.cvut.eshop.shop.Item;
import cz.cvut.eshop.shop.StandardItem;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ItemPurchaseArchiveEntryTest extends AbstractArchiveTest {
    private Item item;
    private static int DEFAULT_COUNT = 1;
    private static int COUNT_3 = 3;
    private static int COUNT_MINUS_3 = -3;


    @Override
    @Before
    public void setUp() {
        item = new StandardItem(1, "apple", (float) 1.2, "electronic", 2);

    }

    @Test
    public void testIncreaseCountHowManyTimesHasBeenSold() {
        ItemPurchaseArchiveEntry itemPurchaseArchiveEntry = new ItemPurchaseArchiveEntry(item);
        Assert.assertEquals("itemPurchaseArchiveEntry should have " + DEFAULT_COUNT + "sold item!", DEFAULT_COUNT, itemPurchaseArchiveEntry.getCountHowManyTimesHasBeenSold());

        itemPurchaseArchiveEntry.increaseCountHowManyTimesHasBeenSold(COUNT_3);

        Assert.assertEquals("itemPurchaseArchiveEntry should have " + DEFAULT_COUNT + COUNT_3 + "sold item!", DEFAULT_COUNT + COUNT_3, itemPurchaseArchiveEntry.getCountHowManyTimesHasBeenSold());

    }


    @Test
    public void testDecreaseCountHowManyTimesHasBeenSold() {
        ItemPurchaseArchiveEntry itemPurchaseArchiveEntry = new ItemPurchaseArchiveEntry(item);
        Assert.assertEquals("itemPurchaseArchiveEntry should have " + DEFAULT_COUNT + "sold item!", DEFAULT_COUNT, itemPurchaseArchiveEntry.getCountHowManyTimesHasBeenSold());

        itemPurchaseArchiveEntry.increaseCountHowManyTimesHasBeenSold(COUNT_MINUS_3);

        Assert.assertEquals("itemPurchaseArchiveEntry should have " + DEFAULT_COUNT + COUNT_MINUS_3 + "sold item!", DEFAULT_COUNT + COUNT_MINUS_3, itemPurchaseArchiveEntry.getCountHowManyTimesHasBeenSold());

    }


    @Test
    public void testCountHowManyTimesHasBeenSoldShouldBeSame() {
        ItemPurchaseArchiveEntry itemPurchaseArchiveEntry = new ItemPurchaseArchiveEntry(item);
        Assert.assertEquals("itemPurchaseArchiveEntry should have " + DEFAULT_COUNT + "sold item!", DEFAULT_COUNT, itemPurchaseArchiveEntry.getCountHowManyTimesHasBeenSold());

        itemPurchaseArchiveEntry.increaseCountHowManyTimesHasBeenSold(0);

        Assert.assertEquals("itemPurchaseArchiveEntry should have " + DEFAULT_COUNT + "sold item!", DEFAULT_COUNT, itemPurchaseArchiveEntry.getCountHowManyTimesHasBeenSold());

    }


}